package pl.uep.kurs.weekend8;

public interface IWspolnyInterfejs {
	
	/**
	 * Wyświetl reprezencaję obiektu w konsoli
	 */
	public void wyswietl();
	
	/**
	 * @return zwraca reprezencację obiektu w csv
	 */
	public String doFormatuCsv();
	
	/**
	 * Wypełnij pola obiektu odpowiednimi wartościami pobranymi z csv
	 * @param wartosci - tablica String z wartościami pól
	 */
	public void wczytajZCsv(String[] wartosci);
	
}
