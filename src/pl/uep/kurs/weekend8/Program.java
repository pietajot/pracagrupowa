package pl.uep.kurs.weekend8;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Program {

	// znak oddzielający wartości w pliku csv
	public static final String ROZDZIELNIK = ";";
	
	public static void main(String[] args) {
		
		// TODO wczytaj plik csv podany jako pierwszy argument, jeśli brak - zakończ program
		
		// TODO dodaj kilka obiektow do listy
		
		// TODO wyswietl wszystkie obiekty w liscie uzywajac metody wyswietl()
		
		// TODO zapisz do pliku
		
	}
	
	/**
	 * Metoda wczytuje elementy z pliku csv
	 * @param sciezka do pliku csv
	 * @return zwraca listę elementów implementujących IWspolnyInterfejs w ArrayList
	 * @throws IOException
	 */
	private static ArrayList<IWspolnyInterfejs> wczytajElementy(String sciezka) throws IOException
	{
		ArrayList<IWspolnyInterfejs> lista = new ArrayList<IWspolnyInterfejs>();
		
		FileReader fr = new FileReader(sciezka);
		BufferedReader czytacz = new BufferedReader(fr);
		String linia = null;
		while((linia = czytacz.readLine()) != null) {
			String[] wartosci = linia.split(Program.ROZDZIELNIK);
			
			String typ = wartosci[0];
			IWspolnyInterfejs obiekt = null;

			// TODO: sprawdzamy typ wedlug ustalonej przez siebie konwencji i tworzymy odpowiednie obiekty
			// np. if (typ=cytrynay {obiekt = new Cytryna()} 
//			if(typ == "ustalonaWartosc") {
//				obiekt = new MojObiekt();
//			}
			
			obiekt.wczytajZCsv(wartosci);
			lista.add(obiekt);
		}
		
		return lista;
	}
	
	/**
	 * Zapisuje listę elementów implementujących IWpolnyInterfejs do pliku csv
	 * @param sciezka do pliku
	 * @param lista elementów do zapisania
	 * @throws IOException
	 */
	private static void zapiszElementy(String sciezka, ArrayList<IWspolnyInterfejs> lista) throws IOException {
		FileWriter fw = new FileWriter(sciezka);
		BufferedWriter pisacz = new BufferedWriter(fw);
		
		for(IWspolnyInterfejs obiekt : lista) {
			pisacz.write(obiekt.doFormatuCsv() + "\r\n");
		}
		
		pisacz.close();
	}

}
